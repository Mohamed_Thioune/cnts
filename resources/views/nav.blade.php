<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <a class="animated slideInDown navbar-brand" href="{{ Route('home.get') }}">
                <img src="{{ asset('assets/images/cntslogo.png') }}" width="40" height="30" alt="Marque|CNTS">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Side left Navbar-->
                <ul class="faster navbar-nav mr-auto">
                    Centre National Tranfusion Sanguine | Sénégal,Dakar
                </ul>

                <!-- Side Right Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Disconnection Links -->
                        {!! Form::open(['route' => 'logout']) !!}
                             {!! Form::submit('Deconnexion', ['class' => 'nav-link-off btn btn-danger animated fadeInDown']) !!}
                        {!! Form::close() !!}
                </ul>
            </div>
        </div>
    </nav>
</body>
</html>
