@extends('../template')
@extends('nav')

@section('head') 
    <title>CNTS|Admin|Users</title>  
    <style> body{ background: url('../assets/images/cntsinterface_admin.png'); background-repeat: no-repeat; } </style>
@endsection

@section('contenu')
<div class="animated pulse block-admin-table col-sm-offset-3 col-sm-6">
    @if (session()->has('ok'))
        <div class="alert alert-success alert-dismissible">{!! session('ok')!!}</div>
    @endif
    <h2 class="title-admin btn btn-danger"><i class="fas fa-cog fa-spin"></i> Espace Administrateur | Vue Admin</h2>
    <div class="panel panel-default">
        <div class="panel-heading"><h2 class="panel-title">Liste des utilisateurs</h2></div>
        <table class="table">
            <thead>
                <tr>
                    <td>#</td>
                    <td colspan="2"><i class="fas fa-arrow-circle-down"></i></span> Nom</td>
                    <td></td>
                    <td></td>
                    <td></td>
                <tr>
            </thead>
            <tbody>
                @foreach ($admin as $user)
                    <tr>
                        <td>{!! $user->id !!}</td>
                        <td class="text-primary">{{ $user->last_name }} {{ $user->name }}</td>
                        <td> {!! link_to_route('Admin.show', '&nbsp;Voir', [$user->id], ['class' => 'btn btn-success glyphicon glyphicon-eye-open']) !!}</td>
                        <td> {!! link_to_route('Admin.edit', '&nbsp;Modifier', [$user->id], ['class' => 'btn btn-warning glyphicon glyphicon-edit']) !!} </td>
                        <td> 
                           {!! Form::open(['method'=>'DELETE', 'route'=>['Admin.destroy', $user->id]]) !!}
                                {!! Form::submit('Supprimer', ['class' => 'btn btn-danger glyphicon glyphicon-trash', 'onclick' => 'return confirm(\'Voulez vous vraiment supprimer cette utilisateur ?\')']) !!}
                           {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {!! link_to_route('Admin.home', '&nbsp;Retour aux questions', [], ['class' => 'btn btn-danger pull-left glyphicon glyphicon-chevron-left'] ) !!}
    {!! link_to_route('Admin.create', '&nbsp;Ajouter un utilisateur', [], ['class' => 'btn btn-primary pull-right glyphicon glyphicon-plus'] ) !!}
    <div class="pull-right">
        {!! $links !!}
    </div>
</div>
@endsection

