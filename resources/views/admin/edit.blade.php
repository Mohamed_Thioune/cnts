@extends('template')
@extends('nav')

@section('title') Admin|Edit @endsection

@section('head')    
    <title>CNTS|Admin|Edit</title>  
    <style> body{ background: url('../assets/images/cntsinterface_admin.png');} </style>
@endsection


@section('etatadmin') <a href="{!! Route('home.get') !!}"><span class="glyphicon glyphicon-off"></span> Deconnexion</a> @endsection

@section('contenu')

<div class="edit col-sm-offset-4 col-sm-4">
    <div class="panel panel-default">
        <div class="panel-heading">
            Modification d'un utilisateur
            <a href="javascript:history.back()" class="btn-back-admin btn btn-primary">
                <span class="glyphicon glyphicon-circle-arrow-left"></span>  Retour
            </a>
        </div>
        {!! Form::model($admin, ['route'=> ['Admin.update', $admin->id] , 'method'=>'put'] ) !!}
            <div class="form-group {!! $errors->has('name') ? 'has-error animated bounce' : '' !!}">
                {!! Form::label('name','Nom :') !!}
                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Veuillez donner votre nom']) !!}
                {!! $errors->first('name','<small class="help-block">:message</small>') !!}
            </div>
            <div class="form-group {!! $errors->has('last_name') ? 'has-error animated bounce' : '' !!}">
                {!! Form::label('last_name','Prenom :') !!}
                {!! Form::text('last_name', null, ['class'=>'form-control', 'placeholder'=>'Veuillez donner votre prenom']) !!}
                {!! $errors->first('last_name','<small class="help-block">:message</small>')!!}
            </div>
            <div class="form-group {!! $errors->has('adresse') ? 'has-error animated bounce' : '' !!}">
                {!! Form::label('adresse','Adresse :') !!}
                {!! Form::text('adresse', null, ['class'=>'form-control', 'placeholder'=>'Veuillez donner votre adresse de domicile']) !!}
                {!! $errors->first('adresse','<small class="help-block">:message</small>')!!}
            </div>
            <div class="form-group {!! $errors->has('telephone') ? 'has-error animated bounce' : '' !!}">
                {!! Form::label('telephone','Telephone :') !!}
                {!! Form::text('telephone', null, ['class'=>'form-control', 'placeholder'=>'Veuillez donner votre numero de telephone']) !!}
                {!! $errors->first('telephone','<small class="help-block">:message</small>')!!}
            </div>
            {!! Form::submit('Envoyer',['class'=>'btn btn-primary pull-right'])!!}
        {!! Form::close() !!}
    </div>

</div>

@endsection