@extends('../template')
@extends('nav')


@section('head')  
    <title>CNTS|Admin|Home</title>  
    <style> body{ background: url('../assets/images/cntsinterface_admin.png'); overflow-x: hidden;} </style>
@endsection


@section('etatadmin') <a href="{!! Route('home.get') !!}"><span class="glyphicon glyphicon-off"></span> Deconnexion</a> @endsection

@section('contenu')
    <div class="animated pulse block-form-admin col-sm-offset-3 col-sm-6 ">
        <div class="panel panel-default">
            <div class="panel-heading">
                Creation d'un utilisateur 
                <a href="javascript:history.back()" class="btn-back-admin btn btn-primary">
                    <span class="glyphicon glyphicon-circle-arrow-left"></span>  Retour
                </a>
            </div>
            <div class="panel-body">
                {!! Form::open(['route'=>'Admin.store', 'class' => 'form-horizontal panel']) !!}
                    <div class="form-group {!! $errors->has('name') ? 'has-error animated bounce' : '' !!}">
                        {!! Form::label('name','Nom :') !!}
                        {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Veuillez donner votre nom']) !!}
                        {!! $errors->first('name','<small class="help-block">:message</small>') !!}
                    </div>
                    <div class="form-group {!! $errors->has('last_name') ? 'has-error animated bounce' : '' !!}">
                        {!!  Form::label('last_name','Prenom :') !!}
                        {!! Form::text('last_name', null, ['class'=>'form-control', 'placeholder'=>'Veuillez donner votre prenom']) !!}
                        {!! $errors->first('last_name','<small class="help-block">:message</small>')!!}
                    </div>
                    <div class="form-group {!! $errors->has('adresse') ? 'has-error animated bounce' : '' !!}">
                        {!!  Form::label('adresse','Adresse :') !!}
                        {!! Form::text('adresse', null, ['class'=>'form-control', 'placeholder'=>'Veuillez donner votre adresse']) !!}
                        {!! $errors->first('adresse','<small class="help-block">:message</small>')!!}
                    </div>
                    <div class="form-group {!! $errors->has('telephone') ? 'has-error animated bounce' : '' !!}">
                        {!! Form::label('telephone','Telephone :') !!}
                        {!! Form::text('telephone', null, ['class'=>'form-control', 'placeholder'=>'Veuillez donner votre telephone']) !!}
                        {!! $errors->first('telephone','<small class="help-block">:message</small>')!!}
                    </div>
                    <div class="form-group {!! $errors->has('email') ? 'has-error animated bounce' : '' !!}">
                        {!! Form::label('email','Em@il :') !!}
                        {!! Form::email('email', null, ['class'=>'form-control', 'placeholder'=>'Veuillez donner votre email']) !!}
                        {!! $errors->first('email','<small class="help-block">:message</small>')!!}
                    </div>
                    <div class="form-group {!! $errors->has('password') ? 'has-error animated bounce' : '' !!}">
                        {!! Form::label('password','Mot de passe :') !!}
                        {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'Veuillez donner votre mot de passe']) !!}
                        {!! $errors->first('password','<small class="help-block">:message</small>') !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('password_confirmation','Ressaisir le mot de passe :') !!}
                        {!! Form::password('password_confirmation',['class'=>'form-control', 'placeholder'=>'Veuillez confirmer votre mot de passe']) !!}
                    </div>
                    {!! Form::submit('save', ['class'=>'btn btn-danger pull-right'] ) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection