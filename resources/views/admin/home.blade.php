@extends('template')
@extends('nav')

@section('head') 
    <title>CNTS|Admin|Home</title>  
    <style> body{ background: url('../assets/images/cntsinterface_admin_home.png');} </style>
@endsection


@section('etatadmin') <a href="{!! Route('home.get') !!}"><span class="glyphicon glyphicon-off"></span> Deconnexion</a>  @endsection


@section('contenu')  
<div class="home-admin">

    <div class="container">
        <h4 class="animated pulse title-admin title-width btn btn-danger"><i class="fas fa-cog fa-spin"></i> Espace Administrateur | Avis clients</h4>
        <h3 class="animated pulse"><a class="vue" href="{!! Route('Admin.homebis') !!}"><i class="fas fa-arrow-circle-right"></i> Passer a une vue par User</a></h3>
        <h4 class="animated fadeInLeft delay-1s"><a class="lien-admin" href="{!! Route('Admin.index') !!}"><i class="fas fa-user-cog"></i> Gérer vos utilisateurs</a></h4>
        <h3>Premiere Question <small>?</small></h3>
        <ul class="responsive-table">
            <li class="animated fadeInLeft table-header faster">
                <div class="col col-1">   <i class="fas fa-sort-down"></i> <i class="fas fa-user"></i> </div>
                <div class="col col-2"> <i class="fas fa-sort-down"></i></span> <i class="fas fa-clipboard-list"></i></div>
            </li>

            @foreach($responses as $response)
                <li class="table-row">
                    <div class="col col-1" data-label="id">{!! $response->id !!}</div>
                    <div class="col col-2" data-label="response">{!! $response->reponse1 !!}</div>
                </li>
            @endforeach
        </ul>
    </div>

    <div class="container">
        <h3>Seconde Question <small>?</small></h3>
        <ul class="responsive-table">
            <li class="table-header">
                <div class="col col-1">  <i class="fas fa-sort-down"></i> <i class="fas fa-user"></i> </div>
                <div class="col col-2"> <i class="fas fa-sort-down"></i></span> <i class="fas fa-clipboard-list"></i>
            </li>

            @foreach($responses as $response)
                <li class="table-row">
                    <div class="col col-1" data-label="id">{!! $response->id !!}</div>
                    <div class="col col-2" data-label="response">{!! $response->reponse2 !!}</div>
                </li>
            @endforeach
        </ul>
    </div>

    <div class="container">
        <h3>Troisieme Question <small>?</small></h3>
        <ul class="responsive-table">
            <li class="table-header">
                <div class="col col-1">  <i class="fas fa-sort-down"></i> <i class="fas fa-user"></i> </div>
                <div class="col col-2"> <i class="fas fa-sort-down"></i></span> <i class="fas fa-clipboard-list"></i>
            </li>

            @foreach($responses as $response)
                <li class="table-row">
                    <div class="col col-1" data-label="id">{!! $response->id !!}</div>
                    <div class="col col-2" data-label="response">{!! $response->reponse3 !!}</div>
                </li>
            @endforeach
        </ul>
    </div>

    <div class="container">
        <h3>Quatrieme Question <small>?</small></h3>
        <ul class="responsive-table">
            <li class="table-header">
                <div class="col col-1">  <i class="fas fa-sort-down"></i> <i class="fas fa-user"></i> </div>
                <div class="col col-2"> <i class="fas fa-sort-down"></i></span> <i class="fas fa-clipboard-list"></i>
            </li>

            @foreach($responses as $response)
                <li class="table-row">
                    <div class="col col-1" data-label="id">{!! $response->id !!}</div>
                    <div class="col col-2" data-label="response">{!! $response->reponse4 !!}</div>
                </li>
            @endforeach
        </ul>
    </div>

    <div class="container">
        <h3>Cinquieme Question <small>?</small></h3>
        <ul class="responsive-table">
            <li class="table-header">
                <div class="col col-1">  <i class="fas fa-sort-down"></i> <i class="fas fa-user"></i> </div>
                <div class="col col-2"> <i class="fas fa-sort-down"></i></span> <i class="fas fa-clipboard-list"></i>
            </li>

            @foreach($responses as $response)
                <li class="table-row">
                    <div class="col col-1" data-label="id">{!! $response->id !!}</div>
                    <div class="col col-2" data-label="response">{!! $response->reponse5 !!}</div>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="pull-right">
        {!! $links !!}
    </div>
</div>
@endsection

