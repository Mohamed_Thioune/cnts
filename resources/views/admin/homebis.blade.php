@extends('template')
@extends('nav')


@section('head')
    <title>CNTS|Admin|Home</title>  
    <style> body{ background: url('../assets/images/cntsinterface_admin_home.png'); } </style>
@endsection
@section('etatadmin') <a href="{!! Route('home.get') !!}"><span class="glyphicon glyphicon-off"></span> Deconnexion</a> @endsection

@section('contenu')
<div class="homebis-admin col-sm-offset-1 col-sm-10">
    <h4 class="animated pulse title-admin title-width btn btn-danger"><i class="fas fa-cog fa-spin"></i> Espace Administrateur | Avis clients</h4>
    <h3 class="animated pulse" ><a class="vue" href="{!! Route('Admin.home') !!}"><i class="fas fa-arrow-circle-right"></i> Passer a une vue par Question</a></h3>
    <h4 class="animated fadeInLeft delay-1s"><a class="lien-admin" href="{!! Route('Admin.index') !!}"><i class="fas fa-user-cog"></i> Gérer vos utilisateurs</a></h4>
    <table class="wrapper">
        <thead class="animated zoomIn">
            <th><h1>#</h1></th>
            <th><h1>&nbsp;Premiere  ?</h1></th>
            <th><h1>Seconde   ?</h1></th>
            <th><h1>Troisieme ?</h1></th>
            <th><h1>Quatrieme ?</h1></th>
            <th><h1>Cinquieme ?</h1></th>
        </thead>

        <tbody>
            @foreach($responses as $response)
                <tr>
                    <td>{!! $response->id !!}&nbsp;</td>
                    <td>{!! $response->reponse1 !!}</td>
                    <td>{!! $response->reponse2 !!}</td>
                    <td>{!! $response->reponse3 !!}</td>
                    <td>{!! $response->reponse4 !!}</td>
                    <td>{!! $response->reponse5 !!}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pull-left">
        {!! $links !!}
    </div>
</div>
@endsection
