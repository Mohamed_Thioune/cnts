@extends('../template')
@extends('nav')

@section('head') 
    <title>CNTS|Admin|Home</title>  
    <style> body{ background: url('../assets/images/cntsinterface_admin.png');} </style>
@endsection

@section('etatadmin') <a href="{!! Route('home.get') !!}"><span class="glyphicon glyphicon-off"></span> Deconnexion</a>  @endsection

@section('contenu')
<div class="fullpage-show">
    <div class="animated bounce block-fiche-users col-sm-offset-3 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading"> <i class="far fa-id-card"></i> Fiche d'utilisateur </div>
            <div class="panel-body">
                <div class="block-image">
                    <h1><i class="far fa-file-image"></i></h1>
                    <img src="" width="" height="">
                </div>
                <div class="block-text">
                    <p> <h2> <i class="fas fa-user-shield"></i> {!! $admin->name !!} {!! $admin->last_name !!}</h2> </p>
                    <p> <div class="address"> {!! $admin->adresse !!} <i class="fas fa-map-marker-alt"></i> </div> </p>
                    <p> <i class="fas fa-envelope"></i> <a href="mailto:{!! $admin->email !!}">{!! $admin->email !!}</a> </p>
                    <p> <i class="fas fa-mobile"></i> {!! $admin->telephone !!} </p>
                    <a class="btn btn-danger" href="{!! Route('Admin.index') !!}">Passer <span class="glyphicon glyphicon glyphicon-ok"></span> </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection