<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
        {!! Html::style('https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css') !!}
        {!! Html::style('https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css') !!}
        <link rel="stylesheet" href="../template_acceuil/assets/css/main.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
        <noscript> <link rel="stylesheet" href="../template_acceuil/assets/css/noscript.css"> </noscript>

        <title>CNTS|REVIEW|Home</title>
    </head>

    <body class="landing is-preload">
        <!-- Page Wrapper -->
        <div id="page-wrapper">
            <!-- Header -->
            <header id="header" class="alt">
                <h1>
                    <a href="{{ Route('home.get') }}"> 
                     <img src="{{ asset('assets/images/cntslogo.png') }}" width="40" height="30" alt="Marque|CNTS">
                     CNTS
                    </a>
                </h1>
                <nav id="nav">
                    <ul>
                        <li class="special">
                            <a href="#menu" class="menuToggle"><span>Menu</span></a>
                            <div id="menu">
                                <ul>
                                    <li><a href="{{ Route('home.get') }}">Home</a></li>
                                    <li><a href="{{ Route('Admin.home') }}">Connexion Admin</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </nav>
            </header>
            
            <!-- Banner -->
            <section id="banner">
                <div class="inner">
                    <h2>CNTS | REVIEW</h2>
                    <p>Le Centre national de transfusion sanguine<br />
                       Solicite votre aide <br />
                       En repondant juste a nos 5 questions <br />
                       Plateforme fourni par <a href="#">BCS IT</a>.</p>
                    <ul class="actions special">
                        <li><a href="#avis-clients" class="button primary scrolly">Commencez !</a></li>
                    </ul>
                </div>
                <a href="#informations" class="more scrolly">En savoir plus</a>
            </section>

            <!-- Informations -->
                <section id="informations" class="wrapper style1 special">
                    <div class="inner">
                        <header class="major">
                            <h2>Assurer la disponibilité et l’accessibilité <br>
                            des différents produits sanguins sur tout le territoire national
                            </h2>
                            <p> 
                                Pour assurer sa mission de la plus belle des manieres la cnts a aujourd'hui besoin de votre aide.<br>
                                Et pour nous aider rien de plus simple, il suffira juste de repondre a ces 5 questions et ainsi nous <br>
                                permettre de prendre en compte vos attentes pour nos prise de décision future.
                            </p>
                        </header>
                        <ul class="icons major">
                            <li><span class="icon fa-user major style1"><span class="label">Lorem</span></span></li>
                            <li><span class="icon solid fa-heart major style3"><span class="label">Ipsum</span></span></li>
                            <li><span class="icon fa-user major style1"><span class="label">Dolor</span></span></li>
                        </ul>
                        <h5>"Le don de sang est tout simplement le don d'une vie a une autre vie"</h5>
                    </div>
                </section>

                <section id="formulaire">
                <span>Votre avis nous importe ...</span>
                <form method="post" action="{{ Route('home.post') }}">
                    {{csrf_field()}}
                    <div class="row gtr-uniform">
                        <!-- Premiére et deuxieme question -->
                        <div class="col-10 {!! $errors->has('reponse1')? 'has-error animated bounce' : '' !!}">
                            <input type="text" name="reponse1" id="premiere-reponse" placeholder="Qu'avez vous pensez de la CNTS apres votre premiére visite ?" />
                            <span class="col-12"> {!! $errors->first('reponse1','<small class="help-block">:message</small>') !!} </span>
                        </div>
                        <div class="col-10 {!! $errors->has('reponse2')? 'has-error animated bounce' : '' !!}">
                            <input type="text" name="reponse2" id="deuxieme-reponse" placeholder="Et comment avez vous jugé l'acceuil qui vous a été porté ?" />
                            <span class="col-12">{!! $errors->first('reponse2','<small class="help-block">:message</small>') !!}</span>
                        </div>

                        <!-- Troisieme question -->
                        <div class="col-10 {!! $errors->has('reponse3')? 'has-error animated bounce' : '' !!}">
                            <select name="reponse3" id="troisieme-reponse">
                                <option value="">- Commment avez-vous connu CNTS ?-</option>
                                <option value="amie">Ami(e)</option>
                                <option value="online">En ligne</option>
                                <option value="visite">Visite</option>
                                <option value="television">Television</option>
                            </select>
                            <span class="col-12">{!! $errors->first('reponse3','<small class="help-block">:message</small>') !!}</span>
                        </div>

                        <!-- Quatrieme question -->
                        <div class="col-12">
                            Quel est votre niveau de satisfaction ?
                        </div>
                        <div class="col-2-xsmall">
                            <input type="radio" id="faible" value="faible" name="reponse4" checked>
                            <label for="faible">Faible 👎</label>
                        </div>
                        <div class="col-2-xsmall">
                            <input type="radio" id="normal-inferieur" value="normal-inferieur" name="reponse4">
                            <label for="normal-inferieur">Pas assez, je dirais 😑</label>
                        </div>
                        <div class="col-2-xsmall">
                            <input type="radio" id="normal-superieur" value="normal-superieur" name="reponse4" checked>
                            <label for="normal-superieur">Oui ca va 😊</label>
                        </div>
                        <div class="col-2-xsmall">
                            <input type="radio" id="eleve" value="eleve" name="reponse4">
                            <label for="eleve">Satisfait a 💯</label>
                        </div>
                        
                    

                        <!-- Cinquieme et derniere question -->
                        <div class="col-10 {!! $errors->has('reponse2')? 'has-error animated bounce' : '' !!}">
                            <textarea name="reponse5" id="demo-message" placeholder="Entrer votre message personnel ici ..." rows="6"></textarea>
                            <span class="col-12">{!! $errors->first('reponse5','<small class="help-block">:message</small>') !!}</span>
                        </div>

                        <!-- Bouton submit & reset -->
                        <div class="col-12">
                            <ul class="actions">
                                <li><input type="submit" value="Envoyer" class="button primary" /></li>
                                <li><input type="reset" value="Annuler" class="button" /></li>
                            </ul>
                        </div>
                        
                    </div>
                </form>
           
            <!-- Footer -->
            <footer id="footer">
                <ul class="icons">
                    <li><a href="#" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="#" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
                    <li><a href="#" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
                    <li><a href="#" class="icon brands fa-dribbble"><span class="label">Dribbble</span></a></li>
                    <li><a href="#" class="icon solid fa-envelope"><span class="label">Email</span></a></li>
                </ul>
                <ul class="copyright">
                    <li>&copy; BCS IT</li><li>Fourni par : <a href="#">Mohamed Thioune</a></li>
                </ul>
            </footer>
        
        </div>

        <!-- Scripts -->
            <script src="../template_acceuil/assets/js/jquery.min.js"></script>
            <script src="../template_acceuil/assets/js/jquery.scrollex.min.js"></script>
            <script src="../template_acceuil/assets/js/jquery.scrolly.min.js"></script>
            <script src="../template_acceuil/assets/js/browser.min.js"></script>
            <script src="../template_acceuil/assets/js/breakpoints.min.js"></script>
            <script src="../template_acceuil/assets/js/util.js"></script>
            <script src="../template_acceuil/assets/js/main.js"></script>

    </body>
</html>