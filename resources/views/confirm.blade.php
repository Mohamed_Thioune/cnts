@extends('template')

@section('head')
    <style> body{ background: url('../assets/images/background_confirm.png'); background-size:cover;} </style>
@endsection

@section('contenu')
    <div class="animated slideInLeft confirm col-sm-offset-2 col-sm-8">
        <div class="panel panel-danger">
            <div class="panel-heading">Reponses transmises</div>
            <div class="panel-body">
                <h2>Merci. Vos reponses ont été enregistrées avec succés ✔️</h2>
            </div>
            <div class="panel-footer">
                Encore merci de votre participation et a bientot❕<br> 
                <a href="{{Route('home.get') }}">Retour vers la page d'ajout <span class="glyphicon glyphicon-circle-arrow-left"></span></a>
            </div>
        </div>
    </div>
@endsection