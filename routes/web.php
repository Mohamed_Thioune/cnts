<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'QuestionController@getInfos', 'as' => 'home.get']);
Route::post('/', ['uses' => 'QuestionController@postInfos', 'as' => 'home.post']);

Route::get('Admin/home', ['uses' => 'QuestionController@listerInfosPerQ', 'as' => 'Admin.home'])->middleware('auth');
Route::get('Admin/homebis', ['uses' => 'QuestionController@listerInfosPerU', 'as' => 'Admin.homebis'])->middleware('auth');

Route::resource('Admin','AdminController')->middleware('auth');
Auth::routes(['register'=>false]);

Route::get('/logout', 'Auth\LoginController@logmeout')->name('logout');
