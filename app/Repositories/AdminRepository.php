<?php

namespace App\Repositories;

use App\User;

class AdminRepository{

    protected $admin;
    
    public function __construct(User $admin){
        $this->admin = $admin;
    }
    
    public function save(User $admin, Array $inputs){
        
        $admin->name = $inputs['name'];
        $admin->last_name = $inputs['last_name'];
        $admin->adresse = $inputs['adresse'];
        $admin->telephone = $inputs['telephone'];

        $admin->save();
    }

    public function getPaginate($n){
        return $this->admin->paginate($n);
    }

    public function store(Array $inputs){  
        $admin = new $this->admin;
        $admin->password = bcrypt($inputs['password']);
        $admin->email = $inputs['email'];

        $this->save($admin,$inputs);

        return $admin;
    }

    public function getById($id){
        return $this->admin->findOrFail($id);
    }

    public function update($id,Array $inputs){
       $this->save($this->getById($id), $inputs);
    }

    public function destroy($id){
        $this->getById($id)->delete();
    }


}