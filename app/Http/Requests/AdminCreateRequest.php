<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100|alpha',
            'last_name' => 'required|max:100|alpha',
            'adresse' => 'required|max:255',
            'telephone' => 'required|min:9|max:9',
            'email' => 'required|email|max:100|unique:users',
            'password' => 'required|confirmed|min:6'
        ];
    }
}
