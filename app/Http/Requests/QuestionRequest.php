<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reponse1'=>'required|min:1|max:100',
            'reponse2'=>'required|min:1|max:100',
            'reponse3'=>'required|min:1|max:100',
            'reponse4'=>'required|min:1|max:100',
            'reponse5'=>'required|min:1|max:100',
        ];
    }
}
