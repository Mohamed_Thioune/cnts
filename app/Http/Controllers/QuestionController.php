<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\QuestionRequest;
use App\Question;

class QuestionController extends Controller
{
    protected $nbrPerPage = 0;
    
    public function listerInfosPerQ(){
        $this->nbrPerPage = 5;
        $responses = Question::where('etat','=','1')->paginate($this->nbrPerPage);
        $links = $responses->render();  
        return view('admin/home', compact('responses','links'));
    }

    public function listerInfosPerU(){
        $this->nbrPerPage = 10;
        $responses = Question::where('etat','=','1')->paginate($this->nbrPerPage);
        $links = $responses->render();  
        return view('admin/homebis', compact('responses','links'));
    }

    public function getInfos(){
        return view('index');
    }


    public function postInfos(QuestionRequest $request)
    {
        $question = new Question;
        
        $question->reponse1 = $request->input('reponse1');
        $question->reponse2 = $request->input('reponse2');
        $question->reponse3 = $request->input('reponse3');
        $question->reponse4 = $request->input('reponse4');
        $question->reponse5 = $request->input('reponse5');
        $question->save();

        return view('confirm');
    }
}
